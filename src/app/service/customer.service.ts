import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable()
export class CustomerService {
  private api =  'http://ec2-13-232-9-30.ap-south-1.compute.amazonaws.com:3000';
  constructor(private _http:HttpClient) { }
  headers;
  token;
  getAll(){
    this.token = localStorage.getItem('token');
    console.log(this.token);
    this.headers = new Headers();
    this.headers.append('Authorization',this.token);
    return this._http.get(this.api + '/customer/all',{headers:this.headers})
  }

  getSingle(id){
    return this._http.get(this.api + '/customer/:id')
  }

  delCustomer(id){
    this.token = localStorage.getItem('token');
    console.log(this.token);
    this.headers = new Headers();
    this.headers.append('Authorization',this.token)
    return this._http.delete(this.api + '/customer/' + id);
  }
}
