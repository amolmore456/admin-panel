import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import 'rxjs/add/operator/map';
import {Router} from '@angular/router';

@Injectable()
export class LoginService {
    private api = 'http://ec2-13-232-9-30.ap-south-1.compute.amazonaws.com:3000';
    private header = new Headers();

    constructor(private _http: HttpClient, private _router: Router) {
    }

    singIn(value) {
        // this.header.append('Content-Type','application/json');
        // console.log(value);
        return this._http.post(this.api + '/user/signIn', (value));
    }

    loggedIn() {
        return !!localStorage.getItem('token');
    }

    getToken() {
        return localStorage.getItem('token');
    }

    logout() {
        localStorage.removeItem('token');
        this._router.navigate(['/']);
    }
}
