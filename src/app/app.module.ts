import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import {AppRoutingModule, RoutingComponent} from './app-routing.module';
import {InterceptorModule} from './service/interceptor.module';
import {TokeninterceptorService} from './service/tokeninterceptor.service';
import {AppComponent} from './app.component';
import {LoginComponent} from './login/login.component';
import {Page404Component} from './page404/page404.component';
import {LoginService} from './service/login.service';
import {CustomerService} from './service/customer.service';
import {AuthGuard} from './auth.guard';
import {NgxPaginationModule} from 'ngx-pagination';
import {FilterPipe} from './search.pipe';

@NgModule({
    declarations: [
        AppComponent,
        LoginComponent,
        RoutingComponent,
        Page404Component,
        FilterPipe

    ],
    imports: [
        BrowserModule,
        // Ng2SmartTableModule,
        AppRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        NgxPaginationModule
    ],
    providers: [
        LoginService,
        CustomerService,
        InterceptorModule,
        AuthGuard,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: TokeninterceptorService,
            multi: true
        }
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}

