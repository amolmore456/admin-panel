import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule,Routes,CanActivate } from '@angular/router';


import {DashboardComponent} from './dashboard/dashboard.component';
import {NavbarComponent} from './dashboard/navbar/navbar.component';
import {MainActivityComponent} from './dashboard/main-activity/main-activity.component';
import {LoginComponent} from './login/login.component';
import {Page404Component} from './page404/page404.component';

// Gards Files 
// import { AuthGardService as AuthGuard } from './service/auth-gard.service';

import { AuthGuard } from './auth.guard';



const routes:Routes = [
  
 
  {
    path:'',
    component:LoginComponent
  },
  {
    path:'dashboard',
    component:DashboardComponent,
    canActivate:[AuthGuard]
  },
  {
    path:'**',
    component:Page404Component
  }
 
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(
      routes,
      // {enableTracing:true}
    )
  ],
  exports:[
    RouterModule
  ],
  declarations: [
    
  ],
  providers:[
    // AuthGuard
  ]
})

export class AppRoutingModule { }


export const RoutingComponent  = [
    LoginComponent,
    DashboardComponent,
    MainActivityComponent,
    NavbarComponent,
    Page404Component
]