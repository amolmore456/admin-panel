import {Component, OnInit, OnDestroy} from '@angular/core';
import {CustomerService,} from '../../service/customer.service';
import {ISubscription} from 'rxjs/Subscription';
import {Router} from '@angular/router';
import {LoginService} from '../../service/login.service';


@Component({
    selector: 'app-main-activity',
    templateUrl: './main-activity.component.html',
    styleUrls: ['./main-activity.component.css']
})
export class MainActivityComponent implements OnInit {
    data = [];
    errMsg: string;
    p = 1;
    searchText;
    private subscription: ISubscription;


    constructor(private customerService: CustomerService, private loginService: LoginService, private router: Router) {
    }

    ngOnInit() {
        this.getAll();
    }


    getAll() {
        this.subscription = this.customerService.getAll()
            .subscribe(
                (succ: any) => {
                    this.data = succ.responce;
                    console.log(succ);

                },
                (err) => {
                    console.log(err.status);
                    if (err.status == 401) {
                        this.errMsg = err.error.message;
                        setTimeout(() => {
                            this.errMsg = null;
                            this.loginService.logout();
                        }, 2000);
                    }
                });
    }

    deleteRowInTable(index) {
        console.log(index);
        this.subscription = this.customerService.delCustomer(index)
            .subscribe(
                (succ: any) => {
                    console.log(succ);
                    this.getAll();
                },
                (err) => {
                    console.log(err);
                }
            );
        console.log(this.data);
    }

    OnDestroy() {
        this.subscription.unsubscribe();
    }
}



